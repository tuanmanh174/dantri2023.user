﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.Services.ApiClient
{
    public class CommentService
    {
        private readonly HttpClient _httpClient;
        public CommentService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<string> AddComment(dynamic request, string token)
        {
            var json = JsonConvert.SerializeObject(request);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            _httpClient.BaseAddress = new Uri("https://localhost:44332");
            var response = await _httpClient.PostAsync("/comment", httpContent);
            var res = await response.Content.ReadAsStringAsync();
            return res;
        }
    }
}
