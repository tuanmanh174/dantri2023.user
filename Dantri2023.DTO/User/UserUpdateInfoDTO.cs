﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DTO.User
{
    public class UserUpdateInfoDTO
    {
        public string fullName { get; set; }
        public DateTime dOB { get; set; }
        public int sex { get; set; }
        public string phoneNumber { get; set; }
        public string address { get; set; }
    }
}
