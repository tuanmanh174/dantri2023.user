﻿using Dantri2023.DTO.User;
using Dantri2023.Services.User;
using Facebook;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.User.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]

    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private IConfiguration _configuration;
        public UserController(IUserService userService, IConfiguration configuration)
        {
            _userService = userService;
            _configuration = configuration;
        }
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] UserLoginRequestDTO request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var resultToken = await _userService.Authenticate(request, 1, "", "");
            return Ok(resultToken);
        }


        [AllowAnonymous]
        [HttpPost("login-facebook")]
        public async Task<IActionResult> FacebookLogin([FromBody] FacebookLoginRequestDTO request)
        {
            try
            {
                FacebookClient fbClient = new FacebookClient(request.AccessToken);

                // Lấy thông tin từ API của Facebook
                dynamic result = fbClient.Get("me", new { fields = "email,name,picture" });

                FacebookUserInfo userInfo = new FacebookUserInfo
                {
                    Email = result.email,
                    Name = result.name,
                    Picture = result.picture.data.url
                };

                // Kiểm tra thông tin người dùng từ Facebook và xử lý đăng nhập

                UserLoginRequestDTO _request = new UserLoginRequestDTO();
                _request.Email = userInfo.Email;
                var restokenultToken = await _userService.Authenticate(_request, 3, userInfo.Name, userInfo.Picture);

                // Trả về token cho người dùng
                return Ok(restokenultToken);
            }
            catch (Exception ex)
            {
                return BadRequest("Login failed: " + ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost("login-google")]
        public async Task<IActionResult> GoogleLogin([FromBody] GoogleLoginRequestDTO request)
        {
            try
            {
                var payload = await GoogleJsonWebSignature.ValidateAsync(request.IdToken, new GoogleJsonWebSignature.ValidationSettings());

                // Kiểm tra thông tin người dùng từ Google và xử lý đăng nhập
                GoogleUserInfo userInfo = new GoogleUserInfo
                {
                    Email = payload.Email,
                    Name = payload.Name,
                    Picture = payload.Picture
                };

                // Thực hiện xác thực và tạo token
                UserLoginRequestDTO _request = new UserLoginRequestDTO();
                _request.Email = userInfo.Email;
                var restokenultToken = await _userService.Authenticate(_request, 2, userInfo.Name, userInfo.Picture);
                // Trả về token cho người dùng
                return Ok(restokenultToken);
            }
            catch (Exception ex)
            {
                return BadRequest("Login failed: " + ex.Message);
            }
        }

        [HttpPost("google-connect")]
        public async Task<IActionResult> GoogleConnect(string idToken)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            string userId = User.Identity.GetId();
            var res = await _userService.GoogleAccountConnect(idToken, userId);
            return Ok(res);
        }

        [HttpPost("facebook-connect")]
        public async Task<IActionResult> FacebookConnect(string accessToken)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            string userId = User.Identity.GetId();
            var res = await _userService.FacebookAccountConnect(accessToken, userId);
            return Ok(res);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] UserRegisterRequestDTO request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var res = await _userService.Register(request);
            return Ok(res);
        }


        [HttpPost("update-user")]
        public async Task<IActionResult> Update([FromBody] UserUpdateInfoDTO request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            string userId = User.Identity.GetId();
            var res = await _userService.UpdateUserAsync(userId, request.fullName, request.dOB, request.sex, request.phoneNumber, request.address);
            return Ok(res);
        }

        [HttpPost("update-password")]
        public async Task<IActionResult> UpdatePassword([FromBody] UserUpdatePasswordDTO request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            string id = User.Identity.GetId();
            var res = await _userService.UpdatePasswordAsync(id, request.PasswordOld, request.PasswordNew);
            return Ok(res);
        }


        [HttpPost("update-email")]
        public async Task<IActionResult> UpdateEmail([FromBody] UserUpdateEmailDTO request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            string id = User.Identity.GetId();
            var res = await _userService.UpdateEmail(request, id);
            return Ok(res);
        }


        [HttpPost("update-display-name")]
        public async Task<IActionResult> UpdateDisplayName([FromBody] UserUpdateDisplayNameDTO request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            string id = User.Identity.GetId();
            var res = await _userService.UpdateDisplayName(request, id);
            return Ok(res);
        }


        [HttpGet("user-detail")]
        public async Task<IActionResult> GetUserInfo()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            string id = User.Identity.GetId();
            var data = await _userService.GetUserInfo(id);
            return Ok(data);
        }

    }

}
