﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DTO
{
    public class Constants
    {
        public const int StatusCodeOk = 200;
        public const int StatusCodeBadRequest = 400;
        public const int StatusCodeUnauthorized = 401;
        public const int StatusCodeForbidden = 403;
        public const int StatusCodeNotFound = 404;
        public const int StatusCodeInternalServerError = 500;
        public const int StatusCodeBadGateway = 502;
        public const int StatusCodeServiceUnavailable = 503;
        public const int StatusCodeGatewayTimeout = 504;
    }
}
