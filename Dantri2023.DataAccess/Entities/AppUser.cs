﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Dantri2023.DataAccess.Entities
{
    public class AppUser : IdentityUser<Guid>
    {
        public string Avatar { get; set; }
        public long AuthorId { get; set; }
        public int Type { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Dob { get; set; }
        public int Sex { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string DisplayName { get; set; }
        public string GoogleEmail { get; set; }
        public string GoogleName { get; set; }
        public string GoogleImage { get; set; }
        public bool IsConnectGoogle { get; set; }
        public bool IsConnectFacebook { get; set; }
        public string FacebookEmail { get; set; }
        public string FacebookName { get; set; }
        public string FacebookImage { get; set; }
    }
}
