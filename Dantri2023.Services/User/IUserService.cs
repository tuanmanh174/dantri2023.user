﻿using Dantri2023.DTO;
using Dantri2023.DTO.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.Services.User
{
    public interface IUserService
    {
        Task<Response> Authenticate(UserLoginRequestDTO request, int Type, string Name, string Image);
        Task<Response> Register(UserRegisterRequestDTO request);
        Task<Response> UpdateUserAsync(string userId, string fullName, DateTime bOD, int sex, string phoneNumber, string address);
        Task<UserDetailDTO> GetUserInfo(string id);
        Task<Response> UpdatePasswordAsync(string id, string oldPassword, string newPassword);
        Task<Response> UpdateEmail(UserUpdateEmailDTO request, string userId);
        Task<Response> UpdateDisplayName(UserUpdateDisplayNameDTO request, string userId);
        Task<Response> GoogleAccountConnect(string idToken, string userId);
        Task<Response> FacebookAccountConnect(string accessToken, string userId);
    }
}
