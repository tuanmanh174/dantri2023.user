﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DTO.User
{
    public class GoogleLoginRequestDTO
    {
        public string IdToken { get; set; }
    }
}
