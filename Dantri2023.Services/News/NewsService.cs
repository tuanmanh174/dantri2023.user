﻿using Dantri2023.DataAccess.EF;
using Dantri2023.DTO;
using Dantri2023.DTO.News;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dantri2023.DataAccess.Entities;

namespace Dantri2023.Services.News
{
    public class NewsService : INewsService
    {
        private readonly DantriDbContext _dbContext;
        public NewsService(DantriDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public List<NewsGetListDTO> GetListNewsByUserId(string userId)
        {
            try
            {
                var lstNews = _dbContext.News.Where(x => x.UserId == userId).ToList();
                var lstData = new List<NewsGetListDTO>();
                foreach (var item in lstNews)
                {
                    var data = new NewsGetListDTO();
                    data.ArticleId = item.ArticleId;
                    data.UserId = item.UserId;
                    lstData.Add(data);
                }
                return lstData;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
                ex.ToString();
                return new List<NewsGetListDTO>();
            }
        }

        public async Task<Response> InsertNewsByUser(NewsCreateDTO request)
        {
            try
            {
                Dantri2023.DataAccess.Entities.News data = new Dantri2023.DataAccess.Entities.News();
                data.ArticleId = request.ArticleId;
                data.Id = new Guid();
                data.UserId = request.UserId;
                data.CreatedDate = DateTime.Now;
                await _dbContext.News.AddAsync(data);
                await _dbContext.SaveChangesAsync();
                return new Response(1000, "Insert Succeed");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
                ex.ToString();
                return new Response(-100, "Insert Failed");
            }
        }
    }
}
