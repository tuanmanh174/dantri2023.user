﻿using Dantri2023.DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DataAccess.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var roleId = new Guid("E97E62F2-E790-46A0-B0F4-B070052544F7");
            var adminId = new Guid("D519FA35-0C44-49CC-B9A7-17B9A2364E8E");
            modelBuilder.Entity<AppRole>().HasData(new AppRole
            {
                Id = roleId,
                Name = "admin",
                NormalizedName = "admin",
                Description = "Administrator Role"
            });

            var hasher = new PasswordHasher<AppUser>();
            modelBuilder.Entity<AppUser>().HasData(new AppUser
            {
                Id = adminId,
                UserName = "admin",
                NormalizedUserName = "admin",
                Email = "manhdt@dantri.net.vn",
                NormalizedEmail = "manhdt@dantri.net.vn",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "123456"),
                SecurityStamp = string.Empty,
                FirstName = "Manh",
                LastName = "Dang Tuan",
                Dob = new DateTime(1990, 04, 17)
            });

            modelBuilder.Entity<IdentityUserRole<Guid>>().HasData(new IdentityUserRole<Guid>
            {
                RoleId = roleId,
                UserId = adminId
            });

            modelBuilder.Entity<AppConfig>().HasData(
                new AppConfig() { Key = "HomeTitle", Value = "This is home page of Dantri" },
                new AppConfig() { Key = "HomeKeyword", Value = "This is keyword of Dantri" },
                new AppConfig() { Key = "HomeDescription", Value = "This is description of Dantri" }
                );


        }
    }
}
