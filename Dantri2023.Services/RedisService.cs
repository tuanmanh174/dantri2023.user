﻿using Dantri2023.DataAccess.EF;
using Dantri2023.DTO.Article;
using Dantri2023.DTO.News;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebGrease;

namespace Dantri2023.Services
{
    public class RedisService : IRedisService
    {
        private readonly IConnectionMultiplexer _redis;
        private readonly IConfiguration _configuration;
        private readonly DantriDbContext _dbContext;
        public RedisService(IConnectionMultiplexer redis, IConfiguration configuration, DantriDbContext dbContext)
        {
            _redis = redis;
            _configuration = configuration;
            _dbContext = dbContext;
        }

        public List<NewGetDetailDTO> GetArticle(string userId)
        {
            try
            {
                var lstArticleUser = _dbContext.News.Where(x => x.UserId == userId).ToList();
                if (lstArticleUser.Count == 0)
                {
                    return new List<NewGetDetailDTO>();
                }
                List<NewGetDetailDTO> news = new List<NewGetDetailDTO>();

                foreach (var item in lstArticleUser)
                {
                    var ab = new NewGetDetailDTO();
                    var cacheKey = string.Format("article_{0}", item.ArticleId); ;
                    var redis = ConnectionMultiplexer.Connect(_configuration["Redis:ConnectionString"]);
                    var database = redis.GetDatabase();

                    var cachedArticle = database.StringGet(cacheKey);
                    var article = JsonConvert.DeserializeObject<Article>(cachedArticle);
                    ab.ArticleId = article.Id;
                    ab.UserId = item.UserId;
                    ab.Title = article.Title;
                    news.Add(ab);
                }
                return news;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return new List<NewGetDetailDTO>();
            }

        }
    }
}
