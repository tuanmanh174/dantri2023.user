﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DTO.User
{
    public class UserRegisterRequestDTO
    {
        public string Password { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public DateTime Dob { get; set; }
        public string Avatar { get; set; }
        public string PhoneNumber { get; set; }
        public int Sex { get; set; }
    }
}
