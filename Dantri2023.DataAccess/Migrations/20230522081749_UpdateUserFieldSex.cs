﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Dantri2023.DataAccess.Migrations
{
    public partial class UpdateUserFieldSex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Sex",
                table: "AppUsers",
                type: "int",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "bit");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("e97e62f2-e790-46a0-b0f4-b070052544f7"),
                column: "ConcurrencyStamp",
                value: "e41a284e-c113-4d71-b2e4-c055826de3c7");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d519fa35-0c44-49cc-b9a7-17b9a2364e8e"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "Sex" },
                values: new object[] { "1eaed9c3-b706-43dd-9aec-c35480bb2a4e", "AQAAAAEAACcQAAAAEAfTnptlUTRdhTV+YhwolzpnX+DHi3MtA1EEastjzvkXd5CL5lLf79MNGvrrbOpxZg==", 0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "Sex",
                table: "AppUsers",
                type: "bit",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("e97e62f2-e790-46a0-b0f4-b070052544f7"),
                column: "ConcurrencyStamp",
                value: "d3a68a81-4e12-4c09-99fa-35a877a0abab");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d519fa35-0c44-49cc-b9a7-17b9a2364e8e"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "Sex" },
                values: new object[] { "5f7e2e4a-8176-4f24-97a3-34c1862a4132", "AQAAAAEAACcQAAAAEHOXAl4wo2X2QPET1pnbSoj2T7NG862hNwWza7r9QhG61GYIvfmHpUix3z5I/7hLkQ==", false });
        }
    }
}
