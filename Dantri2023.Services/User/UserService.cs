﻿using Dantri2023.DataAccess.EF;
using Dantri2023.DataAccess.Entities;
using Dantri2023.DTO;
using Dantri2023.DTO.User;
using Facebook;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.Services.User
{
    public class UserService : IUserService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly DantriDbContext _dbContext;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly IPasswordHasher<AppUser> _passwordHasher;

        public UserService(UserManager<AppUser> userManager, IConfiguration configuration, DantriDbContext dbContext, SignInManager<AppUser> signInManager, IPasswordHasher<AppUser> passwordHasher)
        {
            _userManager = userManager;
            _configuration = configuration;
            _dbContext = dbContext;
            _signInManager = signInManager;
            _passwordHasher = passwordHasher;
        }
        public async Task<Response> Authenticate(UserLoginRequestDTO request, int Type, string Name, string Image)
        {

            try
            {
                AppUser user = new AppUser();
                if (Type == 2)
                {
                    user = _dbContext.Users.Where(x => x.GoogleEmail == request.Email).FirstOrDefault();
                    if (user == null)
                    {
                        user = new AppUser
                        {
                            GoogleEmail = request.Email,
                            Id = new Guid(),
                            GoogleImage = Image,
                            GoogleName = Name,
                            IsConnectGoogle = true
                        };
                        var ins = await _userManager.CreateAsync(user);
                    }
                }
                else if (Type == 3)
                {
                    user = _dbContext.Users.Where(x => x.FacebookEmail == request.Email).FirstOrDefault();
                    if (user == null)
                    {
                        user = new AppUser
                        {
                            FacebookEmail = request.Email,
                            Id = new Guid(),
                            FacebookName = Name,
                            FacebookImage = Image,
                            IsConnectFacebook = true
                        };
                        var ins = await _userManager.CreateAsync(user);
                    }
                }
                else
                {
                    user = _dbContext.Users.Where(x => x.Email == request.Email).FirstOrDefault();
                }

                dynamic data;
                if (user == null && Type == 1)
                {
                    return new Response(-98, "Tài khoản không tồn tại");
                }
                if (Type == 1)
                {
                    var result = await _signInManager.PasswordSignInAsync(user, request.Password, false, true);
                    if (!result.Succeeded)
                    {
                        return new Response(-99, "Đăng nhập không thành công");
                    }
                }


                var roles = await _userManager.GetRolesAsync(user);
                var claims = new[]
                {
                    new Claim(ClaimTypes.Email, user.Email == null ? "" : user.Email),
                    new Claim(ClaimTypes.GivenName, user.FullName == null ? "" : user.FullName),
                    new Claim(ClaimTypes.Role, string.Join(";",roles)),
                    new Claim(ClaimTypes.Name, user.UserName == null ? "" : user.UserName),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(_configuration["JWT:ValidAudience"],
                    _configuration["JWT:ValidAudience"],
                    claims,
                    expires: DateTime.Now.AddDays(3),
                    signingCredentials: creds);

                var _token = new JwtSecurityTokenHandler().WriteToken(token);
                data = new
                {
                    Token = _token
                };

                return new Response(data);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
                ex.ToString();
                return new Response(-99, "Hệ thống đang lỗi, vui lòng liên hệ với admin");
            }

        }

        public async Task<Response> UpdateEmail(UserUpdateEmailDTO request, string userId)
        {
            try
            {
                var checkUser = _dbContext.Users.Where(x => x.Email == request.Email).FirstOrDefault();

                if (checkUser != null)
                {
                    return new Response(-99, "Email tạo mới đã tồn tại trên hệ thống");
                }

                var user = await _userManager.FindByIdAsync(userId);

                var passwordVerificationResult = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, request.Password);


                if (passwordVerificationResult == PasswordVerificationResult.Failed)
                {
                    return new Response(-100, "Mật khẩu không chính xác");
                }


                // Tìm người dùng dựa trên Id
                user.Email = request.Email;

                // Cập nhật thông tin người dùng
                var result = await _userManager.UpdateAsync(user);
                return new Response(100, "Cập nhật thành công");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
                ex.ToString();
                return new Response(-100, "Hệ thống đang lỗi, vui lòng liên hệ admin");
            }
        }

        public async Task<Response> UpdateDisplayName(UserUpdateDisplayNameDTO request, string userId)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId);
                user.DisplayName = request.DisplayName;
                var result = await _userManager.UpdateAsync(user);
                return new Response(100, "Cập nhật tên hiển thị thành công");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
                ex.ToString();
                return new Response(-100, "Hệ thống đang lỗi, vui lòng liên hệ admin");
            }
        }

        public async Task<Response> Register(UserRegisterRequestDTO request)
        {
            try
            {

                var checkEmail = _dbContext.Users.Where(x => x.Email == request.Email).FirstOrDefault();
                if (checkEmail != null)
                {
                    return new Response(-99, "Email tạo mới đã tồn tại trên hệ thống");
                }
                var user = new AppUser
                {
                    Email = request.Email,
                    FullName = request.FullName,
                    Address = request.Address,
                    Dob = request.Dob,
                    Avatar = request.Avatar,
                    PhoneNumber = request.PhoneNumber,
                    Sex = request.Sex,
                    DisplayName = request.FullName
                };

                var result = await _userManager.CreateAsync(user, request.Password);
                if (result.Succeeded)
                {
                    return new Response(100, "Bạn đã đăng ký thành công");
                }
                return new Response(-100, "Bạn đã đăng ký không thành công");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
                ex.ToString();
                return new Response(-99, "Hệ thống đang lỗi, vui lòng liên hệ admin");
            }
        }


        public async Task<Response> UpdateUserAsync(string userId, string fullName, DateTime dOB, int sex, string phoneNumber, string address)
        {
            try
            {

                // Tìm người dùng dựa trên Id
                var user = await _userManager.FindByIdAsync(userId);

                if (user == null)
                {
                    // Người dùng không tồn tại
                    return new Response(-100, "Người dùng không tồn tại");
                }

                // Kiểm tra và cập nhật tên người dùng mới


                user.FullName = fullName;
                user.Sex = sex;
                user.PhoneNumber = phoneNumber;
                user.Address = address;
                user.Dob = Convert.ToDateTime(dOB);

                // Cập nhật thông tin người dùng
                var result = await _userManager.UpdateAsync(user);
                return new Response(100, "Cập nhật thành công");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
                ex.ToString();
                return new Response(-100, "Hệ thống đang lỗi, vui lòng liên hệ admin");
            }

        }


        public async Task<Response> UpdatePasswordAsync(string id, string oldPassword, string newPassword)
        {

            try
            {
                var user = _dbContext.Users.Find(Guid.Parse(id));


                // Kiểm tra xem mật khẩu mới có trùng với mật khẩu cũ hay không
                var passwordVerificationResult = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, oldPassword);

                if (passwordVerificationResult == PasswordVerificationResult.Success)
                {
                    // Mật khẩu mới trùng với mật khẩu cũ
                    var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                    var result = await _userManager.ResetPasswordAsync(user, token, newPassword);
                    return new Response(100, "Cập nhật mật khẩu thành công");
                }
                else if (passwordVerificationResult == PasswordVerificationResult.Failed)
                {
                    return new Response(-100, "Mật khẩu cũ không đúng");
                }
                else
                {
                    // 
                    return new Response(-100, "Có lỗi xảy ra khi kiểm tra mật khẩu");
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                return new Response(-100, "Cập nhật mật khẩu không thành công");
            }

        }

        public async Task<UserDetailDTO> GetUserInfo(string id)
        {
            try
            {
                var user = _dbContext.Users.Find(Guid.Parse(id));
                var userDTO = new UserDetailDTO();
                userDTO.Email = user.Email;
                userDTO.Id = user.Id.ToString();
                userDTO.UserName = user.UserName;
                userDTO.Address = user.Address;
                userDTO.Dob = user.Dob;
                userDTO.FullName = user.FullName;
                userDTO.PhoneNumber = user.PhoneNumber;
                userDTO.Sex = user.Sex;
                return userDTO;
            }
            catch (Exception ex)
            {

                ex.ToString();
                return new UserDetailDTO();
            }

        }

        public async Task<Response> GoogleAccountConnect(string idToken, string userId)
        {
            try
            {
                GoogleJsonWebSignature.ValidationSettings settings = new GoogleJsonWebSignature.ValidationSettings();
                GoogleJsonWebSignature.Payload payload = GoogleJsonWebSignature.ValidateAsync(idToken, settings).Result;

                GoogleUserInfo userInfo = new GoogleUserInfo
                {
                    Email = payload.Email,
                    Name = payload.Name,
                    Picture = payload.Picture
                };
                //Check xem Email đã tồn tại trên hệ thống hay chưa
                var checkEmail = _dbContext.Users.Where(x => x.GoogleEmail == userInfo.Email).FirstOrDefault();
                if (checkEmail != null)
                {
                    return new Response(-99, "Email đã tồn tại trên hệ thống nên không thể đồng bộ");
                }

                var user = _dbContext.Users.Find(Guid.Parse(userId));
                user.GoogleEmail = userInfo.Email;
                user.GoogleName = userInfo.Name;
                user.GoogleImage = userInfo.Picture;
                user.IsConnectGoogle = true;
                await _userManager.UpdateAsync(user);
                return new Response(100, "Cập nhật thành công");
            }
            catch (Exception ex)
            {
                ex.ToString();
                return new Response(-100, "Hệ thống đang lỗi, vui lòng liên hệ admin");
            }
        }


        public async Task<Response> FacebookAccountConnect(string accessToken, string userId)
        {
            try
            {
                FacebookClient fbClient = new FacebookClient(accessToken);

                // Lấy thông tin từ API của Facebook
                dynamic result = fbClient.Get("me", new { fields = "email,name,picture" });

                FacebookUserInfo userInfo = new FacebookUserInfo
                {
                    Email = result.email,
                    Name = result.name,
                    Picture = result.picture.data.url
                };

                //Check xem Email đã tồn tại trên hệ thống hay chưa
                var checkEmail = _dbContext.Users.Where(x => x.FacebookEmail == userInfo.Email).FirstOrDefault();
                if (checkEmail != null)
                {
                    return new Response(-99, "Email đã tồn tại trên hệ thống nên không thể đồng bộ");
                }

                var user = _dbContext.Users.Find(Guid.Parse(userId));
                user.FacebookEmail = userInfo.Email;
                user.FacebookName = userInfo.Name;
                user.FacebookImage = userInfo.Picture;
                user.IsConnectFacebook = true;
                await _userManager.UpdateAsync(user);
                return new Response(100, "Cập nhật thành công");
            }
            catch (Exception ex)
            {
                ex.ToString();
                return new Response(-100, "Hệ thống đang lỗi, vui lòng liên hệ admin");
            }

        }
    }
}
