﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DTO
{
    public static class Enums
    {
        public enum ErrorCode
        {
            NotAuthen = -1001,
            Exception = -99,
            Failed = -1,
            Success = 1
        }
        public enum CAPTCHATYPE
        {
            MATH = 0,
            STRING = 1,
            TOKEN = 2,
            SESSION = 3
        }
    }
}
