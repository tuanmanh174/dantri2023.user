﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DataAccess.EF
{
    public class DantriDbContextFactory : IDesignTimeDbContextFactory<DantriDbContext>
    {
        public DantriDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var connectionString = configuration.GetConnectionString("DantriUserDatabase");
            var optionsBuilder = new DbContextOptionsBuilder<DantriDbContext>();
            optionsBuilder.UseSqlServer(connectionString);
            return new DantriDbContext(optionsBuilder.Options);
        }
    }
}
