﻿using Dantri2023.DTO.Article;
using Dantri2023.DTO.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.Services
{
    public interface IRedisService
    {
        List<NewGetDetailDTO> GetArticle(string userId);
    }
}
