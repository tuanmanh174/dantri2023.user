﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DTO.User
{
    public class UserUpdateDisplayNameDTO
    {
        public string DisplayName { get; set; }
    }
}
