﻿using Dantri2023.DTO.News;
using Dantri2023.Services;
using Dantri2023.Services.News;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dantri2023.User.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NewsController : Controller
    {
        private readonly IRedisService _redisService;
        private readonly INewsService _newService;
        public NewsController(IRedisService redisService, INewsService newService)
        {
            _redisService = redisService;
            _newService = newService;
        }


        [HttpPost("creat-news")]
        public async Task<IActionResult> Create([FromBody] NewsCreateDTO request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var res = await _newService.InsertNewsByUser(request);
            return Ok(res);
        }

        [HttpGet("list-news-by-user")]
        public async Task<IActionResult> Get()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            string id = User.Identity.GetId();
            var data = _redisService.GetArticle(id);
            return Ok(data);
        }
    }
}
