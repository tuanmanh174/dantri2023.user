﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DTO.Article
{
    [ProtoContract]
    public class Article
    {
        public Article()
        {
            RelateNewsIds = new List<long>();
            AllowAd = true;
        }
        [ProtoMember(1)]
        public long Id { get; set; }

        /// <summary>
        /// Tiêu đề của tin bài
        /// </summary>
        [ProtoMember(2)]
        public string Title { get; set; }

        /// <summary>
        /// Mô tả ngắn của tin bài
        /// </summary>
        [ProtoMember(3)]
        public string Sapo { get; set; }

        /// <summary>
        /// Nội dung của tin bài
        /// </summary>
        [ProtoMember(4)]
        public string Body { get; set; }

        /// <summary>
        /// Bút danh của tác giả
        /// </summary>
        [ProtoMember(5)]
        public string Author { get; set; }

        /// <summary>
        /// Danh sách  từ khoá của tin bài
        /// </summary>
        [ProtoMember(6)]
        public List<long> TagIds { get; set; }

        /// <summary>
        /// Link ảnh mô tả tả
        /// Link này sẽ tự động trả về ảnh theo từng kích thước má giao giện yêu cầu
        /// </summary>
        [ProtoMember(7)]
        public string ImageLink { get; set; }

        /// <summary>
        /// Loại tin
        /// 0 tin thường
        /// 13 tin video
        /// 7 cả video cả ảnh
        /// 3 là tin ảnh
        /// </summary>
        [ProtoMember(8)]
        public int NewsType { get; set; }

        /// <summary>
        /// Đường dẫn đến bài chi tiết
        /// </summary>
        [ProtoMember(9)]
        public string LocalPath { get; set; }

        /// <summary>
        /// Danh sách tin liên quan
        /// </summary>
        [ProtoMember(10)]
        public List<long> RelateNewsIds { get; set; }

        /// <summary>
        /// Video của tin bài
        /// </summary>
        [ProtoMember(11)]
        public int VideoId { get; set; }

        /// <summary>
        /// Ngày xuất bản
        /// </summary>
        [ProtoMember(12)]
        public DateTime DistributionDate { get; set; }

        /// <summary>
        /// Id sự kiện của tin bài
        /// </summary>
        [ProtoMember(13)]
        public int CollectionId { get; set; }

        [ProtoMember(14)]
        public string SubTitle { get; set; }

        [ProtoMember(15)]
        public int CategoryId { get; set; }
        [ProtoMember(16)]
        public string DesktopCover { get; set; }
        [ProtoMember(17)]
        public string MobileCover { get; set; }
        [ProtoMember(18)]
        public string VideoCover { get; set; }
        [ProtoMember(19)]
        public string VideoCoverBackground { get; set; }

        [ProtoMember(20)]
        public string TitleAudio { get; set; }
        [ProtoMember(21)]
        public string BodyAudio { get; set; }
        [ProtoMember(22)]
        public string SapoAudio { get; set; }
        [ProtoMember(23)]
        public string ExternalJS { get; set; }
        [ProtoMember(24)]
        public string ExternalCSS { get; set; }
        [ProtoMember(25)]
        public string CreatedBy { get; set; }

        [ProtoMember(27)]
        public bool IsPr { get; set; }

        [ProtoMember(28), DefaultValue(true)]
        public bool AllowAd { get; set; }

        [ProtoMember(29)]
        public string CanonicalURL { get; set; }
        [ProtoMember(30)]
        public string SocialAvatar { get; set; }
        [ProtoMember(31)]
        public string BackgroundColor { get; set; }
        [ProtoMember(32)]
        public string Color { get; set; }
        [ProtoMember(33)]
        public string FontFamily { get; set; }
        [ProtoMember(34)]
        public string HeadingFontFamily { get; set; }
        /// <summary>
        /// Danh sách Id tác giả
        /// </summary>
        [ProtoMember(35)]
        public List<int> AuthorIds { get; set; }
        /// <summary>
        /// Ảnh dọc của bài viết đặc biệt
        /// </summary>
        [ProtoMember(36)]
        public string VerticalImageLink { get; set; }

        /// <summary>
        /// Có hiển thị Donate?
        /// </summary>
        [ProtoMember(37)]
        public bool AllowDonation { get; set; }

        /// <summary>
        /// Nguồn bài viết
        /// </summary>
        [ProtoMember(38)]
        public string SourceName { get; set; }

        /// <summary>
        /// Link Nguồn bài viết
        /// </summary>
        [ProtoMember(39)]
        public string SourceUrl { get; set; }

        /// <summary>
        /// check bài viết lấy lại từ nguồn khác
        /// </summary>
        [ProtoMember(40)]
        public bool IsReup { get; set; }

        [ProtoMember(41)]
        public int CoverLayout { get; set; }

        [ProtoMember(42)]
        public int CoverTheme { get; set; }

        [ProtoMember(43)]
        public string ExternalUrl { get; set; }
    }
}
