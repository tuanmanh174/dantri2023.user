﻿using Dantri2023.DTO;
using Dantri2023.DTO.News;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dantri2023.Services.News
{
    public interface INewsService
    {
        Task<Response> InsertNewsByUser(NewsCreateDTO request);
        List<NewsGetListDTO> GetListNewsByUserId(string userId);
    }
}
