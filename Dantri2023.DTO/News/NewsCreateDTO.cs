﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DTO.News
{
    public class NewsCreateDTO
    {
        public int ArticleId { get; set; }
        public string UserId { get; set; }
    }
}
