﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DataAccess.Entities
{
    public class Notification
    {
        public string UserId { get; set; }
        public long CommentId { get; set; }
        public long ParentId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
