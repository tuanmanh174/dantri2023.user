﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dantri2023.DTO.News
{
    public class NewGetDetailDTO
    {
        public string UserId { get; set; }
        public long ArticleId { get; set; }
        public string Title { get; set; }
    }
}
