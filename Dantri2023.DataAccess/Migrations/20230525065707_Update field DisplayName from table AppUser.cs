﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Dantri2023.DataAccess.Migrations
{
    public partial class UpdatefieldDisplayNamefromtableAppUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "AppUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("e97e62f2-e790-46a0-b0f4-b070052544f7"),
                column: "ConcurrencyStamp",
                value: "b602d94b-f12a-4839-b6f8-b59d8becd8e2");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d519fa35-0c44-49cc-b9a7-17b9a2364e8e"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "f19939fd-546a-4d9d-bf75-198d9cf363f0", "AQAAAAEAACcQAAAAEOX/0zVZoP7vKjVr7rWZP8B3ItMAJyjtLkGTLpK4B7yV4mQCxaZn/g/UWRrKtOx7ow==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "AppUsers");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("e97e62f2-e790-46a0-b0f4-b070052544f7"),
                column: "ConcurrencyStamp",
                value: "a53017f9-7314-4604-8011-af9bd7b70e57");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d519fa35-0c44-49cc-b9a7-17b9a2364e8e"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "6268a146-9eda-4e31-815e-193a98804335", "AQAAAAEAACcQAAAAEKQ87XyulyC7npw4gDbYzX1H5679hl+ZgFIX56ljKQPsCJQzSYiKv6LWsJii1iP9kw==" });
        }
    }
}
