﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Dantri2023.DataAccess.Migrations
{
    public partial class UpdatefieldFacebookInfofromtableAppUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FacebookEmail",
                table: "AppUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FacebookImage",
                table: "AppUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FacebookName",
                table: "AppUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("e97e62f2-e790-46a0-b0f4-b070052544f7"),
                column: "ConcurrencyStamp",
                value: "faa5f698-b2ca-4a8b-92ad-4c4161a6858b");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d519fa35-0c44-49cc-b9a7-17b9a2364e8e"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "337c2de9-163c-4f25-9957-06712e1cb32c", "AQAAAAEAACcQAAAAEGqwe4X3SxODKJeapnpKeUah/KH89cfwmK/rZXNiANaFi/OZzyfC3nigrf4i19nAmw==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FacebookEmail",
                table: "AppUsers");

            migrationBuilder.DropColumn(
                name: "FacebookImage",
                table: "AppUsers");

            migrationBuilder.DropColumn(
                name: "FacebookName",
                table: "AppUsers");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("e97e62f2-e790-46a0-b0f4-b070052544f7"),
                column: "ConcurrencyStamp",
                value: "8b47fa84-401e-488f-8097-d20c9df4bc5c");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d519fa35-0c44-49cc-b9a7-17b9a2364e8e"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "fec51e89-d3f5-4aea-97a1-f067e5e9cc90", "AQAAAAEAACcQAAAAEK4XLKx7UgsmfFB2VFrfsg2gycZYPBIbjPlAaYpjHmHl4DFIFnoyiGVSKCVaxltC3g==" });
        }
    }
}
