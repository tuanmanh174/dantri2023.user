﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Dantri2023.DataAccess.Migrations
{
    public partial class UpdatefieldGoogleInfofromtableAppUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GoogleEmail",
                table: "AppUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GoogleImage",
                table: "AppUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GoogleName",
                table: "AppUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsConnectFacebook",
                table: "AppUsers",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsConnectGoogle",
                table: "AppUsers",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("e97e62f2-e790-46a0-b0f4-b070052544f7"),
                column: "ConcurrencyStamp",
                value: "8b47fa84-401e-488f-8097-d20c9df4bc5c");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d519fa35-0c44-49cc-b9a7-17b9a2364e8e"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "fec51e89-d3f5-4aea-97a1-f067e5e9cc90", "AQAAAAEAACcQAAAAEK4XLKx7UgsmfFB2VFrfsg2gycZYPBIbjPlAaYpjHmHl4DFIFnoyiGVSKCVaxltC3g==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GoogleEmail",
                table: "AppUsers");

            migrationBuilder.DropColumn(
                name: "GoogleImage",
                table: "AppUsers");

            migrationBuilder.DropColumn(
                name: "GoogleName",
                table: "AppUsers");

            migrationBuilder.DropColumn(
                name: "IsConnectFacebook",
                table: "AppUsers");

            migrationBuilder.DropColumn(
                name: "IsConnectGoogle",
                table: "AppUsers");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("e97e62f2-e790-46a0-b0f4-b070052544f7"),
                column: "ConcurrencyStamp",
                value: "b602d94b-f12a-4839-b6f8-b59d8becd8e2");

            migrationBuilder.UpdateData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("d519fa35-0c44-49cc-b9a7-17b9a2364e8e"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "f19939fd-546a-4d9d-bf75-198d9cf363f0", "AQAAAAEAACcQAAAAEOX/0zVZoP7vKjVr7rWZP8B3ItMAJyjtLkGTLpK4B7yV4mQCxaZn/g/UWRrKtOx7ow==" });
        }
    }
}
